module goapi

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.9.0
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
)
