package routers

import (
	"goapi/controllers"

	"github.com/gorilla/mux"
)

// UserRouters for routers
func UserRouters(r *mux.Router) {
	route := r.PathPrefix("/users").Subrouter()
	route.HandleFunc("/", controllers.GetUsers).Methods("GET")
	route.HandleFunc("/", controllers.AddUsers).Methods("POST")
	route.HandleFunc("/", controllers.EditUsers).Methods("PUT")
	route.HandleFunc("/", controllers.DeletUsers).Methods("DELETE")
}

func UsersRoute2() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/books", controllers.GetUsers).Methods("GET")
	r.HandleFunc("/books", controllers.AddUsers).Methods("POST")

	return r
}
