package routers

import (
	"goapi/controllers"

	"github.com/gorilla/mux"
)

// AuthRouters for routers
func AuthRouters(r *mux.Router) {
	route := r.PathPrefix("/auth").Subrouter()
	route.HandleFunc("/signup", controllers.Login).Methods("GET")
	route.HandleFunc("/signin", controllers.SignIn).Methods("GET")
}