package main

import (
	"goapi/configs"
	routers "goapi/routes"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

func initRouters() *mux.Router {
	r := mux.NewRouter()
	routers.UserRouters(r.PathPrefix("/api/v1").Subrouter())
	return r
}

func init() {

	// Load env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Failed Load env")
	} else {
		log.Printf("Prepare application Done.!!")
	}

	// Connect db
	_, dberr := configs.DataBase()
	if dberr != nil {
		log.Fatal("Failed connect" + dberr.Error())
	} else {
		log.Printf("Connected to database success")
	}
}

func main() {
	mainRouters := initRouters()
	
	log.Printf("Service run on port 8080")
	serve := http.ListenAndServe(":8080", mainRouters)

	if serve != nil {
		log.Fatal("Erro when running service", serve)
	}

}



// func mount(r *mux.Router, path string, handler http.Handler) {
//     r.PathPrefix(path).Handler(
//         http.StripPrefix(
//             strings.TrimSuffix(path, "/"),
//             handler,
//         ),
//     )
// }

// func notFound(w http.ResponseWriter, req *http.Request) {
//     _, err := w.Write([]byte("Where yu go?"))

// 	if err == nil {
// 		log.Fatal("Error")
// 	}
// }

// func home(w http.ResponseWriter, req *http.Request) {
//     _, err := w.Write([]byte("hello from API"))
// 	if err == nil {
// 		log.Fatal("Error")
// 	}
// }