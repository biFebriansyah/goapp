package helpers


func Respone(data interface{}, code int, isError bool) interface{} {
	res := map[string]interface{}{
		"status" : code,
		"isError" : isError,
		"data" : data,
	}

	ers := map[string]interface{}{
		"status" : code,
		"isError" : isError,
		"message" : data,
	}

	if isError {
		return ers
	} else {
		return res
	}
}