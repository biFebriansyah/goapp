package repo

import (
	"database/sql"
	"goapi/configs"
	"goapi/models"
)

type initrepo struct {
	db *sql.DB
}

func InitRepo() *initrepo {
	db, _ := configs.DataBase()
	return &initrepo{db}
}

func (r *initrepo) Save(user *models.User) error {
	query := `INSERT INTO public.users("name", username, email, "password", update_at, created_at) VALUES($1, $2, $3, $4, $5, $6)`

	stm, err := r.db.Prepare(query)

	if err != nil {
		return err
	}

	defer stm.Close()

	_, err = stm.Exec(user.Name, user.UserName, user.Email, user.Password, user.UpdateAt, user.CreatedAt)

	if err != nil {
		return err
	}

	return nil
}

func (r *initrepo) FindByID(id string) (*models.User, error) {
	query := `SELECT * FROM public.users WHERE id=$1`

	var users models.User

	stm, err := r.db.Prepare(query)

	if err != nil {
		return nil, err
	}

	defer stm.Close()

	err = stm.QueryRow(id).Scan(&users.Id, &users.Name, &users.UserName, &users.Email, &users.Password, &users.UpdateAt, &users.CreatedAt)

	if err != nil {
		return nil, err
	}

	return &users, nil
}

func (r initrepo) FindAll() (models.Users, error) {
	
	query := `SELECT * FROM public.users`

	rows, err := r.db.Query(query)

	if err != nil {
		return nil, err
	}

	defer rows.Close()


	var data models.Users
	var users models.User

	for rows.Next() {
		err = rows.Scan(&users.Id, &users.Name, &users.UserName, &users.Email, &users.Password, &users.UpdateAt, &users.CreatedAt)

		if err != nil {
			return nil, err
		}

		data = append(data, users)
	}

	return data, nil

}