package configs

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/lib/pq"
)


func DataBase() (*sql.DB, error) {

	host := os.Getenv("PG_HOST")
	user := os.Getenv("PG_USER")
	password := os.Getenv("PG_PASS")
	databaseName := os.Getenv("PG_DNAME")

	config := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable", host, user, password, databaseName)

	db, err := connect(config)

	if err != nil {
		return nil, err
	}

	return db, nil
}

func connect(config string) (*sql.DB, error) {
	db, err := sql.Open("postgres", config)

	if err != nil {
		return nil, err
	}

	db.SetMaxIdleConns(10)
	db.SetMaxOpenConns(10)

	return db, nil
}

