package controllers

import (
	"encoding/json"
	"fmt"
	"goapi/helpers"
	"goapi/models"
	"goapi/repo"
	"net/http"

	"golang.org/x/crypto/bcrypt"
)



func GetUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "Application/json")

	var repos = repo.InitRepo()
	data, err := repos.FindAll()

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	err = json.NewEncoder(w).Encode(helpers.Respone(data, 200, false))

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
}

func AddUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "Application/json")

	var body models.User

    err := json.NewDecoder(r.Body).Decode(&body)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

	Password := []byte(body.Password)
	hashPasword, _ := bcrypt.GenerateFromPassword(Password, bcrypt.DefaultCost)

	data := models.CreateUsers()
	data.Name = body.Name
	data.UserName = body.UserName
	data.Email = body.Email
	data.Password = string(hashPasword)

	var repos = repo.InitRepo()
	err = repos.Save(data)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	err = json.NewEncoder(w).Encode(helpers.Respone(data, 200, false))

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

}


func EditUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "Application/json")
	fmt.Println(r.Method)

	err := json.NewEncoder(w).Encode(map[string]interface{}{
		"status" : 200, 
		"message" : "Masuk Put users",
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

}


func DeletUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "Application/json")
	fmt.Println(r.Method)

	err := json.NewEncoder(w).Encode(map[string]interface{}{
		"status" : 200, 
		"message" : "Masuk Del users",
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

}
