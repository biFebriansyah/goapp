package controllers

import (
	"encoding/json"
	"net/http"
)

// Login users
func Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "Application/json")

	json.NewEncoder(w).Encode(map[string]interface{}{
		"status" : 200, 
		"message" : "Masuk Login",
	})
}

// SignIn method
func SignIn(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "Application/json")

	json.NewEncoder(w).Encode(map[string]interface{}{
		"status" : 200, 
		"message" : "Masuk SignIn",
	})
}